import express from 'express'; 
import json from 'body-parser'; 

export const router = express.Router();

router.get('/', (req, res) => {
    res.render('index', { titulo: "Colegio Patria", nombre: "Alexis Antonio Tirado Ontiveros" });
}); 
router.get('/detallePago', (req, res) => {
    const params = {
    };
    res.render('detallePago', params);
}); 
router.post('/detallePago', (req, res) => {
    const { numDocente, nombre, domicilio, nivel, pagoHoraBase, horasImpartidas, numHijos } = req.body;

    
    let pagoBase = parseFloat(pagoHoraBase);
    if (nivel === '1') {
        pagoBase *= 1.3;
    } else if (nivel === '2') {
        pagoBase *= 1.5;
    } else if (nivel === '3') {
        pagoBase *= 2;
    }
    const pagoPorHoras = pagoBase * parseInt(horasImpartidas);
    const impuesto = pagoPorHoras * 0.16;
    let bono = 0;
    const numHijosInt = parseInt(numHijos);
    if (numHijosInt >= 1 && numHijosInt <= 2) {
        bono = pagoPorHoras * 0.05; 
    } else if (numHijosInt >= 3 && numHijosInt <= 5) {
        bono = pagoPorHoras * 0.1; 
    } else if (numHijosInt > 5) {
        bono = pagoPorHoras * 0.2; 
    }
    const totalPagar = pagoPorHoras + bono - impuesto;

    // Renderizar la vista con los resultados
    res.render('detallePago', {
        numDocente,
        nombre,
        domicilio,
        nivel,
        pagoHoraBase,
        horasImpartidas,
        numHijos,
        pagoPorHoras: pagoPorHoras.toFixed(2),
        pagoIncrementado: bono.toFixed(2),
        impuesto: impuesto.toFixed(2),
        totalPagar: totalPagar.toFixed(2)
    });
});

export default { router };